const express = require("express");
const app = express();
app.use(express.json());

const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport(
  "smtp://" +
    process.env.SMTP_LOGIN +
    ":" +
    process.env.SMTP_PASSWORD +
    "@" +
    process.env.SMTP_HOST
);
const SendEmail = async (req, res) => {
  // get request input
  const { email } = req.body.input;

  // run some business logic
  const fromEmail = "giuseppe.amatulli@student.vinci.be";
  const mailOption = {
    from: fromEmail,
    to: email,
    subjetc: "Xchange Vinci confirmez votre compte",
    html:
      "<p>" +
      "Veuillez cliquer sur le lien suivant afin de confirmer votre adresse mail" +
      "</p>" +
      '<a href="http://google.com">' +
      "clique ici" +
      "</a>",
  };

  transporter.sendMail(mailOption, function (error, info) {
    if (error) {
      return res.status(400).json({
        message: "error happened",
      });
    }
    console.log("Message sent:" + info.response);
    return res.json({
      sucess: "Email sent",
    });
  });

  /*
    // In case of errors:
    return res.status(400).json({
      message: "error happened"
    })
    */

  // success
  return res.json({
    success: "<value>",
  });
};
module.exports = SendEmail;
