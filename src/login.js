const bcrypt = require("bcryptjs");

const { SendQuery, GenerateJWT } = require("./utils");

const Login = async (req, res) => {
  const { email, password } = req.body.input;
  const request = await SendQuery({
    query: `
        query FindUsersByEmail($email: String!){
            users(where: { email: { _eq: $email } }, limit: 1) {
              id
              password
              role
              is_locked
            }
          }`,
    variables: { email },
  });

  const dbUser = request.data.users[0];
  if (!dbUser) return res.status(400).json({ error: "No user found" });

  if (dbUser.is_locked) return res.status(400).json({ error: "User banned" });

  const validPassword = bcrypt.compareSync(password, dbUser.password);
  if (!validPassword) return res.status(400).json({ error: "Invalid" });

  const token = GenerateJWT({
    defaultRole: dbUser.role,
    allowedRoles: [dbUser.role],
    otherClaims: {
      "x-hasura-user-id": `${dbUser.id}`,
    },
  });
  return res.json({ token });
};
module.exports = Login;
