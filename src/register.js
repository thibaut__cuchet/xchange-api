const { SendQuery, GenerateJWT } = require("./utils");
const bcrypt = require("bcryptjs");

const Register = async (req, res) => {
  const user = req.body.input.user;
  user.password = bcrypt.hashSync(user.password);

  const request = await SendQuery({
    query: `
        mutation InsertUser($user: users_insert_input!){
            insert_users_one(object: $user) {
              id
            }
          }`,
    variables: { user },
  });

  if (!request.data)
    return res.status(400).json({ error: "Invalid credentials" });

  const token = GenerateJWT({
    defaultRole: "user",
    allowedRoles: ["user"],
    otherClaims: {
      "x-hasura-user-id": `${request.data.insert_users_one.id}`,
    },
  });

  return res.json({ token });
};
module.exports = Register;
