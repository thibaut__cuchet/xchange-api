const SendEmail = require("./sendMail");
const Login = require("./login");
const Register = require("./register");

const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;
app.use(express.json());

app.post("/sendEmail", SendEmail);

app.post("/login", Login);

app.post("/register", Register);

app.listen(PORT);
