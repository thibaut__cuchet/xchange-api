const fetch = require("cross-fetch");
const jwt = require("jsonwebtoken");

const express = require("express");
const app = express();
app.use(express.json());

const HASURA_ENDPOINT = process.env.HASURA_ENDPOINT;
const HASURA_ADMIN_SECRET = process.env.HASURA_ADMIN_SECRET;
const HASURA_GRAPHQL_JWT_SECRET = process.env.HASURA_GRAPHQL_JWT_SECRET;
const JWT_EXPIRE_TIME = "7d";

const makeGraphQLClient =
  ({ url, headers }) =>
  async ({ query, variables }) => {
    const request = await fetch(url, {
      headers,
      method: "POST",
      body: JSON.stringify({ query, variables }),
    });
    return request.json();
  };

const SendQuery = makeGraphQLClient({
  url: HASURA_ENDPOINT,
  headers: {
    "X-Hasura-Admin-Secret": HASURA_ADMIN_SECRET,
  },
});

const GenerateJWT = ({ allowedRoles, defaultRole, otherClaims }) => {
  const payload = {
    "https://hasura.io/jwt/claims": {
      "x-hasura-allowed-roles": allowedRoles,
      "x-hasura-default-role": defaultRole,
      ...otherClaims,
    },
  };
  return jwt.sign(payload, HASURA_GRAPHQL_JWT_SECRET, {
    algorithm: "HS256",
    expiresIn: JWT_EXPIRE_TIME,
  });
};

module.exports = { SendQuery, GenerateJWT };
